# opus-sys

Projeto Mule com APIs para integração de baixo nível com Opus.

Essa API foi desenhada para atender a camada "System" do padrão de arquitetura [API-led](https://blogs.mulesoft.com/learn-apis/api-led-connectivity/what-is-api-led-connectivity/), portanto é uma API interna (ou de baixo nível) e não deve ser consumida diretamente por parceiros externos.

## Ambiente de desenvolvimento, configuração, compilação e deploy

Usando Maven com [Projetos Mule](https://gitlab.com/pabux/mule/suporte/-/wikis/Ferramentas-desenvolvimento/Compilando-projetos-Mule-com-Maven).

Necessário acesso ao Gitlab via Git e Maven previamente configurados em sua máquina:

  - Acesso com Maven em [Configuração Maven](https://gitlab.com/pabux/mule/suporte/-/wikis/Ferramentas-desenvolvimento/Configura%C3%A7%C3%A3o-settings.xml-pro-Maven)
  - Acesso com Git em [Configuração Git](https://docs.gitlab.com/ee/user/ssh.html)
  
